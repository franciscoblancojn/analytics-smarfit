<?php
/*
Plugin Name: Analytics Smarfit 
Plugin URI: https://gitlab.com/franciscoblancojn/analytics-smarfit
Description: Plugin for Wordpress, for generate Analytics for Smarfit
Author: Francisco Blanco
Version: 1.2.4
Author URI: https://franciscoblanco.vercel.app/
*/

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/franciscoblancojn/analytics-smarfit',
	__FILE__,
	'analytics-smarfit'
);
$myUpdateChecker->setAuthentication('MqiAAZzo9WNBAGzgwc3L');
$myUpdateChecker->setBranch('master');

define("ANSM_LOG",true);
define("ANSM_PATH",plugin_dir_path(__FILE__));
define("ANSM_URL",plugin_dir_url(__FILE__));


require_once ANSM_PATH . "src/_index.php";