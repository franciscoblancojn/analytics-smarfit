<?php

function ANSM_clientes(){
    $args = array(
        'return' => 'ids',
        'meta_key' => 'niubizsuscription',
    );
    
    $users = get_users( $args );
    $head = [
        "ID",
        "Fecha de pago",
        "Nombre",
        "Email",
        "Telefono",
        "Plan",
        "DNI",
    ];
    $products = [];

    $rows = [];

    for ($i=0; $i < count($users); $i++) { 
        $user = $users[$i];
        $user_id = $user->ID;
        $suscription = get_user_meta(
            $user_id,
            "niubizsuscription",
            true
        );

        $productSuscription = wc_get_product( $suscription );
        $product_name = $productSuscription->get_name();
        $products[$product_name]=$product_name;
        $payDate = get_user_meta(
            $user_id,
            "niubizpayDate",
            true
        );

        $rows[] = array(
            "user_id" => array(
                "key"=>"user_id",
                "value"=>$user_id,
                "text"=>'
                    <a href="'.get_admin_url().'user-edit.php?user_id='.$user_id.'">
                    '.$user_id.'
                    </a>
                '
            ),
            "date" => array(
                "key"=>"date",
                "value"=>date("Y-m-d",$payDate),
                "text"=>date("Y-m-d",$payDate)
            ),
            "user_name" => array(
                "key"=>"user_name",
                "value"=>get_user_meta($user_id,"billing_first_name",true),
                "text"=>get_user_meta($user_id,"billing_first_name",true),
            ),
            "email" => array(
                "key"=>"email",
                "value"=>$user->data->user_email,
                "text"=>$user->data->user_email,
            ),
            "telefono" => array(
                "key"=>"telefono",
                "value"=>get_user_meta($user_id,"billing_phone",true),
                "text"=>get_user_meta($user_id,"billing_phone",true),
            ),
            "product" => array(
                "key"=>"product",
                "value"=>$product_name,
                "text"=>'
                    <a href="'.get_admin_url().'post.php?post='.$suscription.'&action=edit">
                    '.$product_name.'
                    </a>
                '
            ),
            "dni" => array(
                "key"=>"dni",
                "value"=>get_user_meta($user_id,"billing_cedula",true),
                "text"=>get_user_meta($user_id,"billing_cedula",true),
            ),
        );
    }
    ?>
    <script>
        const clientes = <?=json_encode($rows,JSON_UNESCAPED_UNICODE)?>;
        const head = <?=json_encode($head)?>;
        const products = Object.values(<?=json_encode($products)?>);
        const headJson = {}
        head.forEach(ele => {
            headJson[ele] = ele
        });
        const clientesCSV = [
            headJson,
            ...clientes.map(e=>{
                const ele = {}
                for (const key in e) {
                    ele[key] = e[key].value
                }
                return ele
            })
        ]
        const data = {
            labels: [
                "Cantidad"
            ],
            datasets: [
                {
                    label: 'Clientes Totales',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: [clientes.length],
                },
                ...products.map((p,i)=>{
                    return {
                        label: `Clientes ${p}`,
                        backgroundColor: `rgb(${50*(i+1)}, 99, 132)`,
                        borderColor: 'rgb(255, 99, 132)',
                        data: [clientes.filter((c)=>c.product.value === p).length],
                    }
                })
            ]
        };
        printANSMgarf({
            type: 'bar',
            data,
            options: {}
        })
        const onDownloadCSV = () => bntDescargarCSV(clientesCSV)
    </script>
    <br>
    <?php
    ANSM_table($head,$rows);
}