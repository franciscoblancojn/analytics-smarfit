<?php

function ANSM_ventasCitas()
{
    if($_POST["ANSM_products_ids_ventasCitas"]){
        update_option("ANSM_products_ids_ventasCitas",$_POST["ANSM_products_ids_ventasCitas"]);
    }
    ?>
    <form method="post">
        <label>
            Productos Ids para Ventas Citas, (Separados por ",")
            <input type="text" name="ANSM_products_ids_ventasCitas" id="ANSM_products_ids_ventasCitas" value="<?=get_option("ANSM_products_ids_ventasCitas")?>">
        </label>
        <button class="button action">
            Save
        </button>
    </form>
    <?php
    function filterVentas($row)
    {
        return array_values(array_filter($row,function($item)
        {
            $products = explode(",",get_option("ANSM_products_ids_ventasCitas"));
            return in_array($item["product"]["product_id"],$products);
        }));
    };
    ANSM_ventas(filterVentas);
}