<?php

function ANSM_ventasByProductNoRecurrencia()
{
    $config = array(
        'limit'=>-1,
        'status'=> array( 'wc-processing' ),
        'return' => 'ids',
        'orderby' => 'date',
        'order' => 'DESC',
        'meta_key'     => 'payRecurrent', // The postmeta key field
        'meta_compare' => 'NOT EXISTS', // The comparison argument
    );
    $config = ANSM_filters_config($config);
    $orders = wc_get_orders($config);

    $head = [
        "Order ID",
        "Date",
        "Usuario",
        "Email",
        "Telefono",
        "Plan",
        "Price",
        "DNI",
    ];

    $all_products_ids = get_posts( array(
        'post_type' => 'product',
        'numberposts' => -1,
        'post_status' => 'publish',
        'fields' => 'ids',
    ));
    $all_products = [];
    foreach ( $all_products_ids as $id ) {
        $product = wc_get_product( $id );
        $all_products[] = array(
            "product_id" => $id,
            "name" => $product->get_name()
        );
    }

    $rows = [];

    for ($i=0; $i < count($orders); $i++) { 
        $order_id = $orders[$i];
        $order = wc_get_order( $order_id );

        foreach ( $order->get_items() as $item_id => $item ) {
            $product_id = $item->get_product_id();
            $product_name = $item->get_name();
        }
        $dni = "";
        $facturaSmarfit = get_post_meta($order_id,"FACSM_factura_smarfit_send",true);
        if($facturaSmarfit){
            $facturaSmarfit = json_decode($facturaSmarfit,true);
            $dni = $facturaSmarfit['DNI'];
        }
        $rows[] = array(
            "order_id" => array(
                "key"=>"order_id",
                "value"=>$order_id,
                "text"=>'
                    <a href="'.get_admin_url().'post.php?post='.$order_id.'&action=edit">
                    '.$order_id.'
                    </a>
                '
            ),
            "date" => array(
                "key"=>"date",
                "value"=>$order->get_date_created()->date('Y-m-d'),
                "text"=>$order->get_date_created()->date('Y-m-d')
            ),
            "user_name" => array(
                "key"=>"user_name",
                "value"=>$order->get_billing_first_name(),
                "text"=>$order->get_billing_first_name(),
            ),
            "email" => array(
                "key"=>"email",
                "value"=>$order->get_billing_email(),
                "text"=>$order->get_billing_email(),
            ),
            "telefono" => array(
                "key"=>"telefono",
                "value"=>$order->get_billing_phone(),
                "text"=>$order->get_billing_phone(),
            ),
            "product" => array(
                "key"=>"product",
                "product_id"=>$product_id,
                "value"=>$product_name,
                "text"=>'
                    <a href="'.get_admin_url().'post.php?post='.$product_id.'&action=edit">
                    '.$product_name.'
                    </a>
                '
            ),
            "price" => array(
                "key"=>"price",
                "value"=>$order->get_total(),
                "text"=>$order->get_formatted_order_total()
            ),
            "dni" => array(
                "key"=>"dni",
                "value"=>$dni,
                "text"=>$dni,
            ),
        );
    }
    ?>
    <script>
        const ventas = <?=json_encode($rows,JSON_UNESCAPED_UNICODE)?>;
        const products = <?=json_encode($all_products,JSON_UNESCAPED_UNICODE)?>;
        const head = <?=json_encode($head)?>;
        const headJson = {}
        head.forEach(ele => {
            headJson[ele] = ele
        });
        const ventasCSV = [
            headJson,
            ...ventas.map(e=>{
                const ele = {}
                for (const key in e) {
                    ele[key] = e[key].value
                }
                return ele
            })
        ]
        const data = {
            labels: meses,
            datasets: [
                {
                    label: "All Products",
                    backgroundColor: `rgb(0, 150, 50)`,
                    borderColor: `rgb(0, 150, 50)`,
                    data: meses.map((e,i)=> 
                        ventas.filter((v)=>(new Date(v.date.value).getMonth() === i)).length
                    ),
                },
                ...products.map((p,i)=>{
                    return {
                        label: p.name,
                        backgroundColor: `rgb(${i*250/products.length}, 99, 132)`,
                        borderColor: `rgb(${i*250/products.length}, 99, 132)`,
                        data: meses.map((e,i)=> 
                            ventas.filter((v)=>(new Date(v.date.value).getMonth() === i) && v.product.product_id == p.product_id).length
                        ),
                    }
                }),
            ]
        };
        printANSMgarf({
            type: 'line',
            data,
            options: {}
        })
        const onDownloadCSV = () => bntDescargarCSV(ventasCSV)
    </script>
    <br>
    <h2>
        Importante, Implementacion de Ventas por Producto (No recurencia) dia 11/04/2022, informacion anterion a dicha fecha si tendra recurrencia 
    </h2>
    <?php
    ANSM_table($head,$rows);
}