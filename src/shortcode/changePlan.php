<?php

function ANSM_changePlan(){
    if(!function_exists("NIUBIZ_get_changePlanReport")){
        echo "Se necesita el plugin Niubiz Payment >= 1.3.1";
        return;
    }
    if($_POST["ANSM_product_id_upgrade"]){
        update_option("ANSM_product_id_upgrade",$_POST["ANSM_product_id_upgrade"]);
    }
    if($_POST["ANSM_product_id_downgrade"]){
        update_option("ANSM_product_id_downgrade",$_POST["ANSM_product_id_downgrade"]);
    }
    ?>
    <form method="post">
        <label>
            Producto Id para Up grade
            <input type="text" name="ANSM_product_id_upgrade" id="ANSM_product_id_upgrade" value="<?=get_option("ANSM_product_id_upgrade")?>">
        </label>
        <label>
            Producto Id para Down grade
            <input type="text" name="ANSM_product_id_downgrade" id="ANSM_product_id_downgrade" value="<?=get_option("ANSM_product_id_downgrade")?>">
        </label>
        <button class="button action">
            Save
        </button>
    </form>
    <?php
    $head = [
        "Fecha",
        "User ID",
        "Usuario",
        "Email",
        "Telefono",
        "Plan Viejo",
        "Plan Nuevo",
        "DNI",
    ];
    $reporst = NIUBIZ_get_changePlanReport();
    $reporst = array_map(function($item){
        $user_id = $item["user_id"];

        $oldPlan_id = $item["oldPlan"];
        if($oldPlan_id == "" || $oldPlan_id == null){
            $oldPlan_name = "productoInvalido";
        }else{
            $oldPlan = wc_get_product( $oldPlan_id );
            $oldPlan_name = $oldPlan->get_name();
        }

        $newPlan_id = $item["newPlan"];
        if($newPlan_id == "" || $newPlan_id == null){
            $newPlan_name = "productoInvalido";
        }else{
            $newPlan = wc_get_product( $newPlan_id );
            $newPlan_name = $newPlan->get_name();
        }


        return array(
            "date" => array(
                "key"=>"date",
                "value"=>date('Y-m-d',strtotime($item["date"])),
                "text"=>date('Y-m-d',strtotime($item["date"]))
            ),
            "user_id" => array(
                "key"=>"user_id",
                "value"=>$user_id,
                "text"=>'
                    <a href="'.get_admin_url().'user-edit.php?user_id='.$user_id.'">
                    '.$user_id.'
                    </a>
                '
            ),
            "user_name" => array(
                "key"=>"user_name",
                "value"=>get_user_meta($user_id,"billing_first_name",true),
                "text"=>get_user_meta($user_id,"billing_first_name",true),
            ),
            "email" => array(
                "key"=>"email",
                "value"=>get_user_meta($user_id,"billing_email",true),
                "text"=>get_user_meta($user_id,"billing_email",true),
            ),
            "telefono" => array(
                "key"=>"telefono",
                "value"=>get_user_meta($user_id,"billing_phone",true),
                "text"=>get_user_meta($user_id,"billing_phone",true),
            ),
            "oldPlan" => array(
                "key"=>"oldPlan",
                "product_id"=>$oldPlan_id,
                "value"=>$oldPlan_name,
                "text"=>'
                    <a href="'.get_admin_url().'post.php?post='.$oldPlan_id.'&action=edit">
                    '.$oldPlan_name.'
                    </a>
                '
            ),
            "newPlan" => array(
                "key"=>"newPlan",
                "product_id"=>$newPlan_id,
                "value"=>$newPlan_name,
                "text"=>'
                    <a href="'.get_admin_url().'post.php?post='.$newPlan_id.'&action=edit">
                    '.$newPlan_name.'
                    </a>
                '
            ),
            "dni" => array(
                "key"=>"dni",
                "value"=>get_user_meta($user_id,"billing_cedula",true),
                "text"=>get_user_meta($user_id,"billing_cedula",true),
            )
        );
    },$reporst);

    if(!empty($_GET["from"])){
        $reporst = array_values(array_filter($reporst,
            function($item)
            {
                return strtotime($item["date"]["value"]) >= strtotime($_GET["from"]);
            }
        ));
    }
    if(!empty($_GET["to"])){
        $reporst = array_values(array_filter($reporst,
            function($item)
            {
                return strtotime($item["date"]["value"]) <= strtotime($_GET["to"]);
            }
        ));
    }
    ?>
    <script>
        const reporst = <?=json_encode($reporst,JSON_UNESCAPED_UNICODE)?>;
        const head = <?=json_encode($head)?>;
        const headJson = {}
        head.forEach(ele => {
            headJson[ele] = ele
        });
        const reporstCSV = [
            headJson,
            ...reporst.map(e=>{
                const ele = {}
                for (const key in e) {
                    ele[key] = e[key].value
                }
                return ele
            })
        ]
        const data = {
            labels: meses,
            datasets: [
                {
                    label: "Up Grades",
                    backgroundColor: '#05a705',
                    borderColor: '#05a705',
                    data: meses.map((e,i)=> 
                        reporst.filter((v)=>(new Date(v.date.value).getMonth() === i) && v.newPlan.product_id == "<?=get_option("ANSM_product_id_upgrade")?>").length
                    ),
                },
                {
                    label: "Down Grades",
                    backgroundColor: 'red',
                    borderColor: 'red',
                    data: meses.map((e,i)=> 
                        reporst.filter((v)=>(new Date(v.date.value).getMonth() === i) && v.newPlan.product_id == "<?=get_option("ANSM_product_id_downgrade")?>").length
                    ),
                }
            ]
        };
        printANSMgarf({
            type: 'line',
            data,
            options: {}
        })
        const onDownloadCSV = () => bntDescargarCSV(reporstCSV)
    </script>
    <br>
    <?php
    ANSM_table($head,$reporst);
}