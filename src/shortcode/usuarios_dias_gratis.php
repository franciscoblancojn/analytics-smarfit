<?php

function ANSM_usuarios_dias_gratis(){
    $head = [
        "Fecha",
        "Dia de Pago",
        "User ID",
        "Usuario",
        "Email",
        "Telefono",
        "Plan",
        "DNI",
    ];
    $config = array(
        'limit'=>-1,
        'status'=> array( 'wc-completed' ),
        'return' => 'ids',
        'orderby' => 'date',
        'order' => 'DESC',
    );
    $orders = wc_get_orders($config);
    
    $reporst = [];
    $dateToday = strtotime(date("Y-m-d"));
    for ($i=0; $i < count($orders); $i++) { 
        $order_id = $orders[$i];
        $order = wc_get_order( $order_id );

        foreach ( $order->get_items() as $item_id => $item ) {
            $product_id = $item->get_product_id();
            $product_name = $item->get_name();
        }
        $diaDePago = -1;
        $email = $order->get_billing_email();
        $user = get_user_by( 'email',$email);
        if(!$user){
            continue;
        }
        $user_id = $user->data->ID;
        $diasGratisProduct = get_post_meta($product_id,"niubiztransactionDiasGratis",true);
        if($diasGratisProduct == "" || $diasGratisProduct == false || $diasGratisProduct == null){
            continue;
        }
        $diaDePago = strtotime($order->get_date_created()->date('Y-m-d')."+ ".$diasGratisProduct." days");

        if($diaDePago <= $dateToday){
            continue;
        }

        $reporst[] = array(
            "date" => array(
                "key"=>"date",
                "value"=>$order->get_date_created()->date('Y-m-d'),
                "text"=>$order->get_date_created()->date('Y-m-d')
            ),
            "diaDePago" => array(
                "key"=>"diaDePago",
                "value"=>date("Y-m-d",$diaDePago),
                "text"=>date("Y-m-d",$diaDePago)
            ),
            "user_id" => array(
                "key"=>"user_id",
                "value"=>$user_id,
                "text"=>'
                    <a href="'.get_admin_url().'user-edit.php?user_id='.$user_id.'">
                    '.$user_id.'
                    </a>
                '
            ),
            "user_name" => array(
                "key"=>"user_name",
                "value"=>get_user_meta($user_id,"billing_first_name",true),
                "text"=>get_user_meta($user_id,"billing_first_name",true),
            ),
            "email" => array(
                "key"=>"email",
                "value"=>get_user_meta($user_id,"billing_email",true),
                "text"=>get_user_meta($user_id,"billing_email",true),
            ),
            "telefono" => array(
                "key"=>"telefono",
                "value"=>get_user_meta($user_id,"billing_phone",true),
                "text"=>get_user_meta($user_id,"billing_phone",true),
            ),
            "product" => array(
                "key"=>"product",
                "product_id"=>$product_id,
                "value"=>$product_name,
                "text"=>'
                    <a href="'.get_admin_url().'post.php?post='.$product_id.'&action=edit">
                    '.$product_name.'
                    </a>
                '
            ),
            "dni" => array(
                "key"=>"dni",
                "value"=>get_user_meta($user_id,"billing_cedula",true),
                "text"=>get_user_meta($user_id,"billing_cedula",true),
            )
        );
    }

    if(!empty($_GET["from"])){
        $reporst = array_values(array_filter($reporst,
            function($item)
            {
                return strtotime($item["date"]["value"]) >= strtotime($_GET["from"]);
            }
        ));
    }
    if(!empty($_GET["to"])){
        $reporst = array_values(array_filter($reporst,
            function($item)
            {
                return strtotime($item["date"]["value"]) <= strtotime($_GET["to"]);
            }
        ));
    }
    ?>
    <style>
        #ANSM-graf{
            display:none;
        }
    </style>
    <script>
        const reporst = <?=json_encode($reporst,JSON_UNESCAPED_UNICODE)?>;
        const head = <?=json_encode($head)?>;
        const headJson = {}
        head.forEach(ele => {
            headJson[ele] = ele
        });
        const reporstCSV = [
            headJson,
            ...reporst.map(e=>{
                const ele = {}
                for (const key in e) {
                    ele[key] = e[key].value
                }
                return ele
            })
        ]
        const data = {
            labels: meses,
            datasets: [{
                label: "Usuarios con dias Gratis",
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: meses.map((e,i)=> 
                    reporst.filter((v)=>(new Date(v.date.value).getMonth() === i)).length
                ),
            }]
        };
        // printANSMgarf({
        //     type: 'line',
        //     data,
        //     options: {}
        // })
        const onDownloadCSV = () => bntDescargarCSV(reporstCSV)
    </script>
    <br>
    <h3>
        Numero de usuarios con dias gratis actualmente <?=count($reporst)?>
    </h3>
    <?php
    ANSM_table($head,$reporst);
}