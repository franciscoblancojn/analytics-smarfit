<?php

function ANSM_pagosRecurrentesCancelados($request = false){
    if(!function_exists("NIUBIZ_get_noPayRecurrentSusciptionReport")){
        echo "Se necesita el plugin Niubiz Payment >= 1.3.9";
        return;
    }
    $head = [
        "Fecha",
        "Ultimo dia de Acceso",
        "User ID",
        "Usuario",
        "Email",
        "Telefono",
        "Plan",
        "DNI",
    ];
    $reporst = NIUBIZ_get_noPayRecurrentSusciptionReport();
    $reporst = array_map(function($item){
        $user_id = $item["user_id"];
        $product_id = $item["plan"];
        if($product_id == "" || $product_id == null){
            $product_name = "productoInvalido";
        }else{
            $product = wc_get_product( $product_id );
            $product_name = $product->get_name();
        }
        return array(
            "date" => array(
                "key"=>"date",
                "value"=>date('Y-m-d',strtotime($item["date"])),
                "text"=>date('Y-m-d',strtotime($item["date"]))
            ),
            "ultimo-dia-acceso" => array(
                "key"=>"ultimo-dia-acceso",
                "value"=>date('Y-m-d',strtotime($item["dateFinal"])),
                "text"=>date('Y-m-d',strtotime($item["dateFinal"]))
            ),
            "user_id" => array(
                "key"=>"user_id",
                "value"=>$user_id,
                "text"=>'
                    <a href="'.get_admin_url().'user-edit.php?user_id='.$user_id.'">
                    '.$user_id.'
                    </a>
                '
            ),
            "user_name" => array(
                "key"=>"user_name",
                "value"=>get_user_meta($user_id,"billing_first_name",true),
                "text"=>get_user_meta($user_id,"billing_first_name",true),
            ),
            "email" => array(
                "key"=>"email",
                "value"=>get_user_meta($user_id,"billing_email",true),
                "text"=>get_user_meta($user_id,"billing_email",true),
            ),
            "telefono" => array(
                "key"=>"telefono",
                "value"=>get_user_meta($user_id,"billing_phone",true),
                "text"=>get_user_meta($user_id,"billing_phone",true),
            ),
            "product" => array(
                "key"=>"product",
                "product_id"=>$product_id,
                "value"=>$product_name,
                "text"=>'
                    <a href="'.get_admin_url().'post.php?post='.$product_id.'&action=edit">
                    '.$product_name.'
                    </a>
                '
            ),
            "dni" => array(
                "key"=>"dni",
                "value"=>get_user_meta($user_id,"billing_cedula",true),
                "text"=>get_user_meta($user_id,"billing_cedula",true),
            )
        );
    },$reporst);


    if(!empty($_GET["from"])){
        $reporst = array_values(array_filter($reporst,
            function($item)
            {
                return strtotime($item["date"]["value"]) >= strtotime($_GET["from"]);
            }
        ));
    }
    if(!empty($_GET["to"])){
        $reporst = array_values(array_filter($reporst,
            function($item)
            {
                return strtotime($item["date"]["value"]) <= strtotime($_GET["to"]);
            }
        ));
    }

    if($request){
        return $reporst;
    }

    ?>
    <script>
        const reporst = <?=json_encode($reporst,JSON_UNESCAPED_UNICODE)?>;
        const head = <?=json_encode($head)?>;
        const headJson = {}
        head.forEach(ele => {
            headJson[ele] = ele
        });
        const reporstCSV = [
            headJson,
            ...reporst.map(e=>{
                const ele = {}
                for (const key in e) {
                    ele[key] = e[key].value
                }
                return ele
            })
        ]
        const data = {
            labels: meses,
            datasets: [{
                label: "Pagos Recurrentes Cancelados",
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: meses.map((e,i)=> 
                    reporst.filter((v)=>(new Date(v.date.value).getMonth() === i)).length
                ),
            }]
        };
        printANSMgarf({
            type: 'line',
            data,
            options: {}
        })
        const onDownloadCSV = () => bntDescargarCSV(reporstCSV)
    </script>
    <br>
    <?php
    ANSM_table($head,$reporst);
}