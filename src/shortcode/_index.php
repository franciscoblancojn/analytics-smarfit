<?php

require_once ANSM_PATH . "src/shortcode/ventas.php";
require_once ANSM_PATH . "src/shortcode/clientes.php";
require_once ANSM_PATH . "src/shortcode/ventasCitas.php";
require_once ANSM_PATH . "src/shortcode/abandonedCart.php";
require_once ANSM_PATH . "src/shortcode/cancelacion.php";
require_once ANSM_PATH . "src/shortcode/changePlan.php";
require_once ANSM_PATH . "src/shortcode/ventasByProduct.php";
require_once ANSM_PATH . "src/shortcode/ventasByProductNoRecurrencia.php";
require_once ANSM_PATH . "src/shortcode/pagosRecurrentCancelados.php";
require_once ANSM_PATH . "src/shortcode/usuarios_dias_gratis.php";