<?php

function ANSM_table($head = [],$rows = [])
{
    ?>
    <table class="wp-list-table widefat fixed striped table-view-list posts">
        <thead>
            <?php
                for ($i=0; $i < count($head); $i++) { 
                    $th = $head[$i];
                    ?>
                    <th scope="col" id="<?=$th?>" class="manage-column column-name column-primary sortable desc tdColmn">
                        <?=$th?>
                    </th>
                    <?php
                }
            ?>
        </thead>
        <tbody>
            <?php
                for ($i=0; $i < count($rows); $i++) { 
                    $row = $rows[$i];
                    ?>
                    <tr id="post-<?=$i?>" class="iedit author-other level-0 type-product status-publish hentry">
                        <?php
                        foreach ($row as $key => $td) {
                            ?>
                            <td key="<?=$key?>" class="tdColmn">
                                <?=$td["text"]?>
                            </td>
                            <?php
                        }
                        ?>
                    </tr>
                    <?php
                }
            ?>
        </tbody>
    </table>
    <?php
}