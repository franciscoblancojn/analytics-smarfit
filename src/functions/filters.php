<?php

function ANSM_filters(){
    ?>
        <form method="get">
            <?php
                foreach ($_GET as $key => $value) {
                    ?>
                        <input type="hidden" name="<?=$key?>" value="<?=$value?>">
                    <?php
                }
            ?>
            <label>
                From
                <input type="date" name="from" id="from" value="<?=$_GET["from"]?>">
            </label>
            <label>
                To
                <input type="date" name="to" id="to" value="<?=$_GET["to"]?>">
            </label>
            <label>
                Min
                <input type="number" name="min" id="min" value="<?=$_GET["min"]?>">
            </label>
            <label>
                Max
                <input type="number" name="max" id="max" value="<?=$_GET["max"]?>">
            </label>
            <button class="button action">
                Filtrar
            </button>
        </form>

    <?php
}
function ANSM_filters_config($config){
    if(!empty($_GET["from"])){
        $config["date_created"] = '>=' . $_GET["from"];
    }
    if(!empty($_GET["to"])){
        $config["date_created"] = '<=' . $_GET["to"];
    }
    if(!empty($_GET["to"]) && !empty($_GET["from"])){
        $config["date_created"] = $_GET["from"]."...".$_GET["to"];
    }
    return  $config;
}