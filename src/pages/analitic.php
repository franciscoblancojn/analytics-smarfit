<?php
function ANSM_create_menu() {

	//create new top-level menu
	add_menu_page('Analytics Smarfit', 'Analytics Smarfit', 'administrator', __FILE__, 'ANSM_settings_page' , ANSM_URL.'src/img/ANSM.png' );

	//call register settings function
	add_action( 'admin_init', 'register_ANSM_settings' );
}
add_action('admin_menu', 'ANSM_create_menu');


function register_ANSM_settings() {
	//register our settings
	register_setting( 'ANSM-settings-group', 'new_option_name' );
	register_setting( 'ANSM-settings-group', 'some_other_option' );
	register_setting( 'ANSM-settings-group', 'option_etc' );
}

function ANSM_settings_page() {
    ?>
    <link rel="stylesheet" href="<?=ANSM_URL?>src/css/style.css">
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <a href="" id="ANSMbtnDonwload" hidden></a>
    <div class="wrap ANSM">
        <h1>
            Analiticas Smarfit
        </h1>
        <div class="container">
            <div class="menu">
                <form method="get" class="fromMenu">
                    <?php
                        foreach ($_GET as $key => $value) {
                            ?>
                                <input type="hidden" name="<?=$key?>" value="<?=$value?>">
                            <?php
                        }
                        $btns = [
                            "Ventas",
                            "Clientes",
                            "Venta de citas",
                            "Venta por Producto",
                            "Venta por Producto (No Recurencia)",
                            "Pedido de cancelación",
                            "Cancelación automática",
                            "Usuarios con Dias Gratis",
                            "Cambio de Plan",
                            "Carrito abandonado",
                        ];

                        for ($i=0; $i < count($btns); $i++) { 
                            $btn = $btns[$i];
                            ?>
                            <input name="contentAnalitic" type="submit" value="<?=$btn?>" class="btn" <?=$_GET["contentAnalitic"] == $btn ? "active" : ""?>>
                            <?php
                        }
                    ?>
                </form>
            </div>
            <div class="content">
                <canvas id="ANSM-graf"></canvas>
                <script src="<?=ANSM_URL?>src/js/main.js"></script>
                <br>
                <br>
                <br>
                <?php
                    if($_GET["contentAnalitic"]){
                        ?>
                        <div class="contentFiltersAndDonwload">
                            <button class="button action" onclick="onDownloadCSV()">
                                Descargar CSV
                            </button>
                            <?php
                            ANSM_filters()
                            ?>
                        </div>
                        <?php
                    }
                
                ?>
                <br>
                <?php
                    switch ($_GET["contentAnalitic"]) {
                        case 'Ventas':
                            ANSM_ventas();
                            break;
                        case 'Clientes':
                            ANSM_clientes();
                            break;
                        case 'Venta de citas':
                            ANSM_ventasCitas();
                            break;
                        case 'Venta por Producto':
                            ANSM_ventasByProduct();
                            break;
                        case 'Venta por Producto (No Recurencia)':
                            ANSM_ventasByProductNoRecurrencia();
                            break;
                        case 'Carrito abandonado':
                            ANSM_abandonedCart();
                            break;
                        case 'Pedido de cancelación':
                            ANSM_cancelacion();
                            break;
                        case 'Cancelación automática':
                            ANSM_pagosRecurrentesCancelados();
                            break;
                        case 'Cambio de Plan':
                            ANSM_changePlan();
                            break;
                        case 'Usuarios con Dias Gratis':
                            ANSM_usuarios_dias_gratis();
                            break;
                        
                        default:
                            echo "";
                            break;
                    }
                
                ?>
            </div> 
        </div>
    </div>
    <?php 
}