const ANSMgraf = document.getElementById("ANSM-graf");
const meses = [
  "Enero",
  "Febrero",
  "Marzo",
  "Abril",
  "Mayo",
  "Junio",
  "Julio",
  "Agosto",
  "Septiembre",
  "Octubre",
  "Noviembre",
  "Diciembre",
];
const printANSMgarf = (config) => new Chart(ANSMgraf, config);
const ConvertToCSV = (objArray) => {
  var array = typeof objArray != "object" ? JSON.parse(objArray) : objArray;

  var CSV = "";

  for (var i = 0; i < array.length; i++) {
    var line = "";
    for (var index in array[i]) {
      if (line != "") line += ",";

      line += array[i][index];
    }

    CSV += line + "\r\n";
  }

  return "data:text/csv;charset=utf-8," + escape(CSV);
};
const bntDescargarCSV = (json) => {
  const btnDonwload = document.getElementById("ANSMbtnDonwload");
  btnDonwload.href = ConvertToCSV(json);
  btnDonwload.download = new Date().toDateString() + ".csv";
  btnDonwload.click();
};
