<?php
if(ANSM_LOG){
    function add_ANSM_LOG_option_page($admin_bar)
    {
        global $pagenow;
        $admin_bar->add_menu(
            array(
                'id' => 'ANSM_LOG',
                'title' => 'ANSM_LOG',
                'href' => get_site_url().'/wp-admin/options-general.php?page=ANSM_LOG'
            )
        );
    }

    function ANSM_LOG_option_page()
    {
        add_options_page(
            'Log Analytics Smarfit',
            'Log Analytics Smarfit',
            'manage_options',
            'ANSM_LOG',
            'ANSM_LOG_page'
        );
    }

    function ANSM_LOG_page()
    {
        if($_GET["user_id"]!= "" && $_GET["user_id"]!= null){
            $fecha_actual = date("d-m-Y");
            update_user_meta(
                $_GET["user_id"],
                "ANSMpayDate",
                strtotime($fecha_actual)
            );
        }
        $log = get_option("ANSM_LOG");
        if($log === false || $log == null || $log == ""){
            $log = "[]";
        }
        ?>
        <script>
            const log = <?=$log?>;
        </script>
        <h1>
            Solo se guardan las 100 peticiones
        </h1>
        <pre><?php var_dump(array_reverse(json_decode($log,true)));?></pre>
        <?php
    }
    add_action('admin_bar_menu', 'add_ANSM_LOG_option_page', 100);

    add_action('admin_menu', 'ANSM_LOG_option_page');

}

function addANSM_LOG($newLog)
{
    if(!ANSM_LOG){
        return;
    }
    $log = get_option("ANSM_LOG");
    if($log === false || $log == null || $log == ""){
        $log = "[]";
    }
    $log = json_decode($log);
    $log[] = $newLog;
    $log = array_slice($log, -100,100); 
    update_option("ANSM_LOG",json_encode($log));
}