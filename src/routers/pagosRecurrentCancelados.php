<?php
require_once(preg_replace('/wp-content.*$/','',__DIR__).'wp-load.php');

$data = json_decode(file_get_contents('php://input'), true);
if(isset($data)){
    $_POST = $data;
}

function ANSM_requestRouter()
{
    $email = $_GET["email"];
    if(empty($email)){
        return json_encode(array(
            "status" => 400,
            "error" => "email required",
        ));
    }

    $reports = ANSM_pagosRecurrentesCancelados(true);
    
    $reports = array_map(function($item){
        $newItem = [];
        foreach ($item as $key => $value) {
            $newItem[$key] = $value["value"];
        }
        if($newItem["email"] == $_GET["email"]){
            echo json_encode(array(
                "status" => 200,
                "data" => $newItem,
            ));
            exit;
        }
        return [];
    },$reports);

    return json_encode(array(
        "status" => 400,
        "error" => "email invalid",
    ));
}
echo ANSM_requestRouter();